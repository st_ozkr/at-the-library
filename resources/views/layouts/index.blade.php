<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Biblioteca</title>
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('css/styles.css')}}"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
  </head>
  <body>
    <div class="">
      <nav class="navbar navbar-expand-lg" style="margin-left: 950px;position: fixed;">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
          </ul>
          <div class="my-2 my-lg-0">
            <button type="button" class="btn-search" data-toggle="modal" data-target="#login">
              iniciar sesion
            </button>
            <button type="button" class="btn-search" data-toggle="modal" data-target="#register">
              registrarse
            </button>
          </div>
        </div>
      </nav>
    </div>
	@include('sweet::alert')
  @yield('content')
  <!-- Modal login-->
<div class="modal fade" id="login" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <form class="form-login" action="{{route('iniciarsesion')}}" method="post">
          @csrf
          <input type="text" name="" value="" hidden>
          <input type="text" class="login-input" name="email" placeholder="email" required>
          <input type="password" class="login-input" name="password" placeholder="password" required>
          <center>
            <button type="submit"  class="btn-form">iniciar sesion</button>
          </center>
        </form>
      </div>
    </div>
  </div>
</div>

  <!-- Modal register-->
<div class="modal fade" id="register" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <form class="form-login" action="{{route('registrarse')}}" method="post">
          @csrf
          <input type="text" name="" value="" hidden>
          <input type="text" class="login-input" name="email" placeholder="email" required>
          <input type="text" class="login-input" name="usuario" placeholder="usuario" required>
          <input type="password" class="login-input" name="password" placeholder="password" required>
          <center>
            <div class="g-recaptcha" data-sitekey="6LdTePgUAAAAAGaBu1kvmcaF1QPH_RNeM4txuKnH"></div>
            <button type="submit"  class="btn-form">Registrarse</button>
          </center>
        </form>
      </div>
    </div>
  </div>
</div>
  </body>
</html>
