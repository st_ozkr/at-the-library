<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Dashboard Internautas</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="{{asset("assets/css/bootstrap.min.css")}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{asset("assets/css/animate.min.css")}}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{asset("assets/css/light-bootstrap-dashboard.css?v=1.4.0")}}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{asset("assets/css/demo.css")}}" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{asset("assets/css/pe-icon-7-stroke.css")}}" rel="stylesheet" />

		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>

@include('sweet::alert')

<div class="wrapper">
    <div class="sidebar" data-color="white" data-image="img/fondo.jpg">
    	<div class="sidebar-wrapper">
            <div class="logo">
              <center>
                {{$user->name}}
              </center>
            </div>
          	<ul class="nav">
							{{-- <li>
									<a href="{{route('dashboard')}}">
											<i class="fa fa-home"></i>
											<p>Inicio</p>
									</a>
							</li> --}}
                <li>
									  <a type="button" data-toggle="modal" data-target="#addBook">
                        <i class="fa fa-book"></i>
                        <p>añadir libro</p>
                    </a>
                </li>
								{{-- <li>
									  <a  id="vventa" type="button">
                        <i class="fa fa-book"></i>
                        <p>Ver Ventas</p>
                    </a>
                </li> --}}
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    {{-- <a class="navbar-brand" href="#">Dashboard</a> --}}
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-left">
                        <li>
                            {{-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-dashboard"></i>
								<p class="hidden-lg hidden-md">Dashboard</p>
                            </a> --}}
                        </li>
                        <li class="dropdown">
                              {{-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-globe"></i>
                                    <b class="caret hidden-lg hidden-md"></b>
									<p class="hidden-lg hidden-md">
										5 Notifications
										<b class="caret"></b>
									</p>
                              </a> --}}
                              {{-- <ul class="dropdown-menu">
                                <li><a href="#">Notification 1</a></li>
                                <li><a href="#">Notification 2</a></li>
                                <li><a href="#">Notification 3</a></li>
                                <li><a href="#">Notification 4</a></li>
                                <li><a href="#">Another notification</a></li>
                              </ul> --}}
                        </li>
                        <li>

                           {{-- <a href="">
                                <i class="fa fa-search"></i>
								<p class="hidden-lg hidden-md">Search</p>
                            </a> --}}
                        </li>
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                           {{-- <a href="">
                               <p>Account</p>
                            </a> --}}
															<button id="vventas" type="button">ventas</button>
                        </li>
                        {{-- <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <p>
										Dropdown
										<b class="caret"></b>
									</p>

                              </a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something</a></li>
                                <li class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                              </ul>
                        </li> --}}
                        <li>

                            <a href="{{route('salir')}}">
                                <p>Salir</p>
                            </a>
                        </li>
												<li class="separator hidden-lg"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
					<div class="row">
					@if (isset($libros) && $libros->count() > 0)
						@foreach ($libros as $libro)
								<div class="col-md-3 pr-1">
									<center>
										<a data-toggle="modal" data-target="#infoBook{{$libro->id}}"><img src="{{asset('/imgBooks/'.$libro->imagen)}}" width="100" height="150px" alt=""></a> <br><br>
										{{$libro->nombre}}<br>
										@switch($libro->disponible)
											@case(0)
											<div class="alert alert-success" role="alert">
												$ disponible $
											</div>
											@break
											@case(1)
											<div class="alert alert-warning" role="alert">
												$ apartado $
											</div>
											@break
											@case(2)
											<div class="alert alert-danger" role="alert">
												$ vendido $
											</div>
											@break
										@endswitch
										<form class="" action="{{route('delete-book')}}" method="post">
											@csrf
											<input type="text" name="idBookDelete" value="{{$libro->id}}" hidden>
											<button type="submit" class="btn btn-danger">Eliminar</button>
										</form>
									</center>
								</div>

								{{-- //info book --}}
								<div class="modal fade" id="infoBook{{$libro->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"  style"z-index: 100000">
									<div class="modal-dialog modal-dialog-centered" role="document">
										<div class="modal-content">
											<div class="modal-body">
												<form class="" action="{{route('update-book')}}" method="post" enctype="multipart/form-data">
													@csrf
													<input type="text" name="id" value="{{$libro->id}}" hidden>
													<div class="row">
														<div class="col-md-6 pr-1">
															<div class="form-group">
																<label>Nombre</label>
																<input name="nombre" type="text" class="form-control" value="{{$libro->nombre}}">
															</div>
														 </div>
														 <div class="col-md-6 pr-1">
															<div class="form-group">
																<label>Area</label>
																<select id="area2" name="area" class="form-control">
																	<option value="" selected>-- seleccionar --</option>
																	<option value="computacion">Computacion</option>
																	<option value="hardware">Hardware</option>
																	<option value="matematicas">Matematicas</option>
																	<option value="fisica">Fisica</option>
																	<option value="otros">Otros</option>
																</select>
															</div>
														</div>
														<div class="col-md-12 pr-1">
															<div class="form-group">
																<label>Description</label>
																<textarea name="descripcion" class="form-control" rows="8" cols="80">{{$libro->descripcion}}</textarea>
															</div>
														</div>
														<div class="col-md-6 pr-1">
															<div class="form-group">
																<label>Costo</label>
																<input name="costo" type="text" class="form-control" value="{{$libro->costo}}">
															</div>
														</div>
														<div class="col-md-6 pr-1">
															<div class="form-group">
																<label>Estado actual</label>
																<select id="estado2" name="estado" class="form-control">
																	<option value="" selected>-- seleccionar --</option>
																	<option value="impecable">Impecable</option>
																	<option value="regular">Regular</option>
																	<option value="maltratado">Maltratado</option>
																</select>
															</div>
														</div>
														<div class="col-md-12 pr-1">
															<h4>Portada actual del libro actual</h4>
															<img src="{{asset('/imgBooks/'.$libro->imagen)}}" width="100" height="150px" alt="">
															<br>
														</div>
														<div class="col-md-12 pr-1">
															<div class="form-group">
																<label>Imagen</label>
																<input accept="image/*" name="imagen" type="file" class="form-control" placeholder="elegir">
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
															<button type="submit" class="btn-form">Guardar cambios</button>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>
								</div>
								<script>
									document.ready = document.getElementById("area2").value = '{{$libro->area}}';
									document.ready = document.getElementById("estado2").value = '{{$libro->estado}}';
								</script>
						@endforeach
					@else
						<div id="alerta" class="alert alert-warning">
							<strong>¡No tienes libros registrados!.</strong>
						</div>
				  @endif
					</div>
					<hr style="width:100%">
					<div id="libros">

					</div>
        </div>

        <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            {{-- <a href="#">
                                Home
                            </a> --}}
                        </li>
                        <li>
                            {{-- <a href="#">
                                Company
                            </a> --}}
                        </li>
                        <li>
                            {{-- <a href="#">
                                Portfolio
                            </a> --}}
                        </li>
                        <li>
                            {{-- <a href="#">
                               Blog
                            </a> --}}
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> JOR, Aplicaciones web
                </p>
            </div>
        </footer>

    </div>
</div>

<!-- Modal add book-->
<div class="modal fade" id="addBook" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role="document">
	<div class="modal-content">
		<div class="modal-body">
  	<form id="newBook" enctype="multipart/form-data" method="post">
				<div class="row">
          <div class="col-md-6 pr-1">
            <div class="form-group">
            	<label>Nombre</label>
            	<input name ="nombre" type="text" class="form-control" placeholder="Mobi-duck" required>
						</div>
           </div>
					 <div class="col-md-6 pr-1">
						<div class="form-group">
							<label>Area</label>
							<select class="form-control" name="area" required>
								<option value="" selected>-- seleccionar --</option>
								<option value="computacion">Computacion</option>
								<option value="hardware">Hardware</option>
								<option value="matematicas">Matematicas</option>
								<option value="fisica">Fisica</option>
								<option value="otros">Otros</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 pr-1">
						<div class="form-group">
							<label>Description</label>
							<textarea name="descripcion" class="form-control" placeholder="descripcion..." rows="8" cols="80" required></textarea>
						</div>
					</div>
					<div class="col-md-6 pr-1">
						<div class="form-group">
							<label>Costo</label>
							<input name="costo" type="text" class="form-control" placeholder="$0.00" required>
						</div>
					</div>
					<div class="col-md-6 pr-1">
						<div class="form-group">
							<label>Estado actual</label>
							<select class="form-control" name="estado" required>
								<option value="" selected>-- seleccionar --</option>
								<option value="impecable">Impecable</option>
								<option value="regular">Regular</option>
								<option value="maltratado">Maltratado</option>
							</select>
						</div>
					</div>
					<div class="col-md-12 pr-1">
						<div class="form-group">
							<label>Imagen</label>
							<input accept="image/*" name="imagen" type="file" class="form-control" placeholder="elegir" required>
						</div>
					</div>
					<center>
						<button type="submit" id="add-book" class="btn-form">añadir libro</button>
					</center>
				</div>
			</form>
		</div>
	</div>
</div>
</div>

<script type="text/javascript">
$(document).ready(function(e) {
	$('#alerta2').hide();
	$('#add-book').click(function(e){
		e.preventDefault();
		var book = new FormData(document.getElementById("newBook"));
		$.ajax({
			type: "POST",
			url: '{{ route ('add-book')}}',
			data: book,
			cache: false,
			contentType: false,
			processData: false,
			headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
			success: function(result)
			{

				$('#addBook').modal('hide');
				// let data = JSON.parse(result);
					 console.log(result);
						let template = '';
					// data.forEach(data => {
							 template += `<h4>Añadido recientemente</h4><img src="../imgBooks/${result.imagen}" width="100" height="150px" alt=""><br>${result.nombre} `;
          // });
          $('#libros').html(template);
				// console.log(data.msg);
				let frase = "Libro agregado";
				Swal.fire(
					frase,
					'',
					'success'
				)
				$('#alerta').hide();
				// location.reload();
			},
			error: function(err) {
				console.log(err);
				let frase = err;
				Swal.fire(
					frase,
					'hubo un error intenta de nuevo',
					'error'
				)
			}
		});
	});
});
</script>

</body>

    <!--   Core JS Files   -->
  <script src="assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>


    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>


</html>
