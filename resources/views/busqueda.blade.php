@extends('layouts.index')

@section('content')

  <section class="busqueda">

  @if (isset($books) && $books->count() > 0)
    <div id="nuevaBusqueda" class="content-search">

        <form class="form-inline" action="{{route('buscar')}}" method="post">
          @csrf
          <input class="form-control mr-sm-2" type="search" placeholder="Nombre Libro" aria-label="Search" name="buscar">
          <button class="btn-search my-2 my-sm-0" type="submit">Buscar</button>
        </form>

    </div>
    <div class="row justify-content-center content-result">
        <div class="col-auto">
          <table class="table table-responsive">
            <thead>
              <tr>
                <td><button  id="nBusqueda" type="button" class="btn btn-primary">Nueva Busqueda</button></td>
                <td>Nombre</td>
                <td>Precio</td>
                <td>Estado</td>
                <td>Accion</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($books as $libro)
                <tr>
                  <td><img src="{{asset('/imgBooks/'.$libro->imagen)}}" width="70" height="100px" alt=""></td>
                  <td>{{$libro->nombre}}</td>
                  <td>$ {{number_format($libro->costo,2)}}</td>
                  <td>{{$libro->estado}}</td>
                  <td><button type="button" class="btn btn-success" data-toggle="modal" data-target="#infoBook{{$libro->id}}">Ver</button> <button type="button" class="btn btn-warning" style="color:#fff;" data-toggle="modal" data-target="#venta{{$libro->id}}">Adquirir</button></td>
                </tr>
                {{-- info book --}}
                <div class="modal fade" id="infoBook{{$libro->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"  style"z-index: 100000">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-body">

                          <div class="row">
                            <div class="col-md-12 pr-1">
                              <center>
                                <img src="{{asset('/imgBooks/'.$libro->imagen)}}" width="100" height="150px" alt="">
                              <center>
                              <br>
                            </div>
                            <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" value="{{$libro->nombre}}" readonly>
                              </div>
                             </div>
                             <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Area</label>
                                <input type="text" class="form-control" value="{{$libro->area}}" readonly>
                              </div>
                            </div>
                            <div class="col-md-12 pr-1">
                              <div class="form-group">
                                <label>Description</label>
                                <textarea class="form-control" rows="8" cols="80" readonly>{{$libro->descripcion}}</textarea>
                              </div>
                            </div>
                            <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Costo</label>
                                <input type="text" class="form-control" value="$ {{number_format($libro->costo,2)}}" readonly>
                              </div>
                            </div>
                            <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Estado actual</label>
                                <label>Nombre</label>
                                <input type="text" class="form-control" value="{{$libro->estado}}" readonly>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              <button type="button" class="btn-form" data-dismiss="modal" data-toggle="modal" data-target="#venta{{$libro->id}}">Adquirir</button>
                            </div>
                          </div>

                      </div>
                    </div>
                  </div>
                </div>

                {{-- venta --}}
                <div class="modal fade" id="venta{{$libro->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"  style"z-index: 100000">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-body">
                        <form class="" action="{{route('adquirir')}}" method="post">
                          @csrf
                          <input type="text" name="id" value="{{$libro->id}}" hidden>
                          <div class="row">
                            <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Matricula</label>
                                <input type="text" name="matricula" class="form-control" required placeholder="201529891">
                              </div>
                             </div>
                             <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Carrera</label>
                                <input type="text" name="carrera" class="form-control" required placeholder="Ing. Computacion">
                              </div>
                            </div>
                            <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Telefono</label>
                                <input type="text" name="telefono" class="form-control"  required placeholder="2222222222">
                              </div>
                            </div>
                            <div class="col-md-6 pr-1">
                              <div class="form-group">
                                <label>Emil</label>
                                <input type="text" name="mail" class="form-control"  required placeholder="correo@example.com">
                              </div>
                            </div>
                            <div class="col-md-12 pr-1">
                              <div class="form-group">
                                <label>Punto de Entrega</label>
                                <textarea name="encuentro" class="form-control" rows="4" cols="80" required placeholder="Parque acualita"></textarea>
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                              <button type="submit" class="btn-form">Adquirir</button>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
  @else
    <div class="content-search">
      <p style="color: #fff;"> No ahi libros relacionados a tu busqueda <br> Nueva busqueda?</p>
      <form class="form-inline" action="{{route('buscar')}}" method="post">
        @csrf
        <input class="form-control mr-sm-2" type="search" placeholder="Nombre Libro" aria-label="Search" name="buscar">
        <button class="btn-search my-2 my-sm-0" type="submit">Buscar</button>
      </form>
    </div>
    @endif
  </section>
  <script type="text/javascript">
  $(document).ready(function(e) {
    $('#nuevaBusqueda').hide();
    $('#nBusqueda').click(function(e){
      $('#nuevaBusqueda').show();
    });
  });
  </script>
@stop
