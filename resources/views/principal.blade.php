@extends('layouts.index')

@section('content')
  <section class="index">
    <div class="content-search">
      <form class="form-inline" action="{{route('buscar')}}" method="post">
        @csrf
        <input class="form-control mr-sm-2" type="search" placeholder="Nombre Libro" aria-label="Search" name="buscar">
        <button class="btn-search my-2 my-sm-0" type="submit">Buscar</button>
      </form>
    </div>
  </section>
@stop
