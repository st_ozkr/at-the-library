@extends('layouts.dash')

@section('content ')
  <div class="row">
  @if (isset($libros) && $libros->count() > 0)
    @foreach ($libros as $libro)
        <div class="col-md-3 pr-1">
          <center>
            <a data-toggle="modal" data-target="#infoBook{{$libro->id}}"><img src="{{asset('/imgBooks/'.$libro->imagen)}}" width="100" height="150px" alt=""></a> <br><br>
            {{$libro->nombre}}<br>
            @switch($libro->disponible)
              @case(0)
              <div class="alert alert-success" role="alert">
                $ disponible $
              </div>
              @break
              @case(1)
              <div class="alert alert-warning" role="alert">
                $ apartado $
              </div>
              @break
              @case(2)
              <div class="alert alert-danger" role="alert">
                $ vendido $
              </div>
              @break
            @endswitch
            <form class="" action="{{route('delete-book')}}" method="post">
              @csrf
              <input type="text" name="idBookDelete" value="{{$libro->id}}" hidden>
              <button type="submit" class="btn btn-danger">Eliminar</button>
            </form>
          </center>
        </div>

        {{-- //info book --}}
        <div class="modal fade" id="infoBook{{$libro->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"  style"z-index: 100000">
          <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <form class="" action="{{route('update-book')}}" method="post" enctype="multipart/form-data">
                  @csrf
                  <input type="text" name="id" value="{{$libro->id}}" hidden>
                  <div class="row">
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Nombre</label>
                        <input name="nombre" type="text" class="form-control" value="{{$libro->nombre}}">
                      </div>
                     </div>
                     <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Area</label>
                        <select id="area2" name="area" class="form-control">
                          <option value="" selected>-- seleccionar --</option>
                          <option value="computacion">Computacion</option>
                          <option value="hardware">Hardware</option>
                          <option value="matematicas">Matematicas</option>
                          <option value="fisica">Fisica</option>
                          <option value="otros">Otros</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Description</label>
                        <textarea name="descripcion" class="form-control" rows="8" cols="80">{{$libro->descripcion}}</textarea>
                      </div>
                    </div>
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Costo</label>
                        <input name="costo" type="text" class="form-control" value="{{$libro->costo}}">
                      </div>
                    </div>
                    <div class="col-md-6 pr-1">
                      <div class="form-group">
                        <label>Estado actual</label>
                        <select id="estado2" name="estado" class="form-control">
                          <option value="" selected>-- seleccionar --</option>
                          <option value="impecable">Impecable</option>
                          <option value="regular">Regular</option>
                          <option value="maltratado">Maltratado</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-md-12 pr-1">
                      <h4>Portada actual del libro actual</h4>
                      <img src="{{asset('/imgBooks/'.$libro->imagen)}}" width="100" height="150px" alt="">
                      <br>
                    </div>
                    <div class="col-md-12 pr-1">
                      <div class="form-group">
                        <label>Imagen</label>
                        <input accept="image/*" name="imagen" type="file" class="form-control" placeholder="elegir">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                      <button type="submit" class="btn-form">Guardar cambios</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <script>
          document.ready = document.getElementById("area2").value = '{{$libro->area}}';
          document.ready = document.getElementById("estado2").value = '{{$libro->estado}}';
        </script>
    @endforeach
  @else
    <div id="alerta" class="alert alert-warning">
      <strong>¡No tienes libros registrados!.</strong>
    </div>
  @endif
  </div>
  <hr style="width:100%">
  <div id="libros">

  </div>
@stop
