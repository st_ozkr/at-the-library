<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class book extends Model
{
    public function propietario()
    {
      return $this->belongsTo('App\User', 'id_user');
    }

    public function venta()
    {
      return $this->hasMany('App\Venta', 'book_id');
    }
}
