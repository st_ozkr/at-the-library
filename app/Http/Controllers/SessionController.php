<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\book;
use App\Venta;
use Alert;
use Auth;

class SessionController extends Controller
{
    public function dashboard()
    {
       $user = Auth::user();
       $libros = $user->libros;
       alert()->success('Bienvenido', ':)!');
       return view('layouts.dash', compact('user','libros'));
    }

    public function login(Request $request)
    {
      $user = User::where('email', $request->email)->first();
      if (isset($user)) {
        if ($user->password == $request->password) {
          Auth::login($user);
          return redirect()->route('dashboard');
        }else{
          alert()->error('Intentalo nuevamente :)!','contraseña incorrecta');
          return redirect()->route('index');
        }
      } else {
        alert()->warning('en nuestros registros :)!','Email no existe');
        return redirect()->route('index');
      }
    }

    public function register(Request $request)
    {
      $validate1 = User::where('email',$request->email)->first();
      $validate2 = User::where('name',$request->usuario)->first();

      if (isset($validate1)) {
        alert()->warning('Email registrado', 'Intenta con otro :)!');
        return redirect()->route('index');
      }elseif (isset($validate2)) {
        alert()->warning('Usuario registrado', 'Intenta con otro :)!');
        return redirect()->route('index');
      }else {
        $usuario = new User();
        $usuario->name = $request->usuario;
        $usuario->email = $request->email;
        $usuario->password = $request->password;
        $usuario->save();
        Auth::login($usuario);
        return redirect()->route('dashboard');
      }
    }

    public function salir()
    {
      Auth::logout();
      alert()->info('Hasta luego','sesion cerrada');
      return redirect()->route('index');
    }

    public function addBook(Request $request)
    {
      $file = $request->file('imagen');
      $name = time().$file->getClientOriginalName();
      $file->move(public_path().'/imgBooks/',$name);

      $user = Auth::user();
      $book = new book();
      $book->nombre = $request->nombre;
      $book->area = $request->area;
      $book->descripcion = $request->descripcion;
      $book->costo = $request->costo;
      $book->estado = $request->estado;
      $book->imagen = $name;
      $book->id_user = $user->id;
      $book->disponible = 0;
      $book->save();
      return $book;
    }

    public function updateBook(Request $request)
    {
      $user = Auth::user();
      if (isset($request->imagen)) {
        $file = $request->file('imagen');
        $name = time().$file->getClientOriginalName();
        $file->move(public_path().'/imgBooks/',$name);

        $book = book::find($request->id);
        $book = new book();
        $book->nombre = $request->nombre;
        $book->area = $request->area;
        $book->descripcion = $request->descripcion;
        $book->costo = $request->costo;
        $book->estado = $request->estado;
        $book->imagen = $name;
        $book->id_user = $user->id;
        $book->save();
        $libros = $user->libros;
        alert()->success('Libro actualizado', ':)!');
        return view('layouts.dash', compact('user','libros'));
      }else {
        $book = book::find($request->id);
        $book->nombre = $request->nombre;
        $book->area = $request->area;
        $book->descripcion = $request->descripcion;
        $book->costo = $request->costo;
        $book->estado = $request->estado;
        $book->id_user = $user->id;
        $book->save();
        $libros = $user->libros;
        alert()->success('Libro actualizado', ':)!');
        return view('layouts.dash', compact('user','libros'));
      }
    }

    public function deleteBook(Request $request)
    {
      $user = Auth::user();
      $book = book::find($request->idBookDelete);
      $book->delete();
      $libros = $user->libros;
      alert()->success('Libro borrado', ':(!');
      return view('layouts.dash', compact('user','libros'));
    }

    public function buscar(Request $request)
    {
      $books = book::where('disponible', 0)
          ->where('nombre', 'like', '%' . $request->buscar . '%')
					->orWhere('area', 'like', '%' . $request->buscar . '%')
					->orWhere('descripcion', 'like', '%' . $request->buscar . '%')
          ->get();
      return view('busqueda', compact('books'));
    }

    public function venta(Request $request)
    {
      $venta = new Venta();
      $venta->book_id = $request->id;
      $venta->matricula = $request->matricula;
      $venta->carrera = $request->carrera;
      $venta->telefono = $request->telefono;
      $venta->mail = $request->mail;
      $venta->encuentro = $request->encuentro;
      $venta->save();
      $book = book::find($request->id);
      $book->disponible = 1;
      $book->save();
      alert()->success('el dueño se pondra en contacto contigo','Libro apartado');
      return redirect()->route('index');
    }
}
