<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
  public function libro()
  {
    return $this->belongsTo('App\book', 'book_id');
  }
}
