<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//index
Route::get('/', function () {
    return view('principal');
})->name('index');

//sesion
Route::get('/dashboard','SessionController@dashboard')->name('dashboard');
Route::post('/login','SessionController@login')->name('iniciarsesion');
Route::post('/register','SessionController@register')->name('registrarse');
Route::get('/logout','SessionController@salir')->name('salir');

//libros
Route::post('/add-book','SessionController@addBook')->name('add-book');
Route::post('/update-book','SessionController@updateBook')->name('update-book');
Route::post('/delete-book','SessionController@deleteBook')->name('delete-book');

//buscar libro
Route::post('/buscar','SessionController@buscar')->name('buscar');
Route::post('/buy-book','SessionController@venta')->name('adquirir');

Route::get('/prueba', function () {
    return view('prueba');
})->name('dash');
